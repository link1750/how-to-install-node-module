### 如何安裝 node module

#### 使用方式：

- 透過 npm 社群
    - npm install {module name}
- 解除安裝
    - npm uninstall {module name}

```sh
$ npm install express

$ npm uninstall express
```

- 自訂 module
    - 使用本機端檔案安裝
    - npm install {your module folder path}/{module name}
- 解除安裝
    - npm uninstall {module name}

```sh
$ npm install ./self_modules/modemo

$ npm uninstall modemo
```

P.S. 範例中的 `self_modules` 可以自訂為自己喜歡的路徑

---

#### 自訂 module

- 建立 module 用 folder
- 建立 package.json
    - npm init -y
    - 預設進入點為 index.js
    - 物件匯出語法 `module.exports = {function|object|...}`

```javascript
/*--- 程式範例 ---*/

var demo = {};
demo.print = function(text) { console.log(text); }

module.exports = demo;
```

---

#### 檔案結構範例

##### 檔案結構

```
./project
|
|- package.json
|- index.js
|
|- self_modules
|   |- modemo
|       |- package.json
|       |- index.js
|       |- lib
|           |- demo.js
|
|- node_modules
|   |- npm module ...
```

##### 重點檔案

###### ./project/self_modules/modemo/index.js

```javascript
module.exports = require('./lib/demo.js');
```

######  ./project/self_modules/modemo/lib/demo.js

```javascript
var demo = {};

demo.print = function(text) {
    message = 
        typeof text == 'string'
          ? ('Your Input Message is ' + text) 
          : 'Please Input Text.';

    console.log(message);
};

module.exports = demo;
```

###### ./project/index.js

```javascript
var demo = require('demo');

demo.print('text');
```
